FROM maven:3-eclipse-temurin-21 as build
WORKDIR /build
COPY src src
COPY pom.xml .

RUN mvn package

FROM eclipse-temurin:21-alpine
WORKDIR /app
COPY --from=build /build/target/java_dns.jar app.jar

ENV PORT=2053
ENV RESOLVER=""

EXPOSE $PORT

ENTRYPOINT java -jar /app/app.jar --port $PORT --resolver $RESOLVER
