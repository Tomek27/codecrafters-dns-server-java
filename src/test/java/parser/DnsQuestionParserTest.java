package parser;

import converter.ByteArray;
import dns.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.List;

import static converter.ByteArray.fromString;
import static dns.DnsClass.IN;
import static dns.DnsType.A;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * <a href="https://www.rfc-editor.org/rfc/rfc1035#section-4.1.4">RFC</a>
 * <p>
 * For example, a datagram might need to use the domain names F.ISI.ARPA,
 * FOO.F.ISI.ARPA, ARPA, and the root.  Ignoring the other fields of the
 * message, these domain names might be represented as in the following methods.
 * <p>
 * The domain name for F.ISI.ARPA is shown at offset 20.  The domain name
 * FOO.F.ISI.ARPA is shown at offset 40; this definition uses a pointer to
 * concatenate a label for FOO to the previously defined F.ISI.ARPA.  The
 * domain name ARPA is defined at offset 64 using a pointer to the ARPA
 * component of the name F.ISI.ARPA at 20; note that this pointer relies on
 * ARPA being the last label in the string at 20.  The root domain name is
 * defined by a single octet of zeros at 92; the root domain name has no
 * labels.
 */
class DnsQuestionParserTest {

  /**
   * OFF|                                               | HEX
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 00 |         Packet Identifier (ID)                | 04D2
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 02 |QR| OPCODE    |AA|TC|RD|RA|   Z    |  RCODE    | 0100
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 04 |         Question Count (QDCOUNT)              | 0003
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 06 |         Answer Record Count (ANCOUNT)         | 0000
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 08 |         Authority Record Count (NSCOUNT)      | 0000
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 10 |         Additional Record Count (ARCOUNT)     | 0000
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   */
  private static byte[] dnsHeader() {
    var header = DnsHeader.builder()
        .packetIdentifier((short) 1234)
        .questionCount((short) 4)
        .build();
    return ByteArray.fromDnsHeader(header);
  }

  /**
   * OFF|                                               | HEX
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 12 |           1           |           F           | 0146
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 14 |           3           |           I           | 0349
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 16 |           S           |           I           | 5349
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 18 |           4           |           A           | 0441
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 20 |           R           |           P           | 5250
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 22 |           A           |          NULL         | 4100
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 24 |                  Type A = 1                   | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 26 |                 Class IN = 1                  | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   */
  private static byte[] questionFIsiArpa() {
    DnsQuestion questionUncompressed = new DnsQuestion(A, IN, "f", "isi", "arpa");
    return ByteArray.fromDnsQuestion(questionUncompressed);

  }

  /**
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 28 |           3           |           F           | 0346
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 30 |           O           |           O           | 4F4F
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 32 | 1  1|            PTR -> 12                    | C012
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 34 |                  Type A = 1                   | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 36 |                 Class IN = 1                  | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   */
  private static byte[] questionFoo() {
    ByteBuffer buf = ByteBuffer.allocate(10);
    buf.put((byte) 3);
    buf.put(fromString("foo"));
    buf.put((byte) -64); // PTR -> 12
    buf.put((byte) 12); // PTR -> 12
    buf.putShort(A.value());
    buf.putShort(IN.value());
    return buf.array();
  }

  /**
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 38 | 1  1|             PTR -> 18                   | C012
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 40 |                  Type A = 1                   | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 42 |                 Class IN = 1                  | 0001
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   */
  private static byte[] questionArpa() {
    ByteBuffer buf = ByteBuffer.allocate(6);
    buf.put((byte) -64); // PTR -> 18
    buf.put((byte) 18); // PTR -> 18
    buf.putShort(A.value());
    buf.putShort(IN.value());
    return buf.array();
  }

  /**
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 92 |           0           |       Typ             | 0000
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 94 |        A = 1          |     Class             | 0100
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   * 96 |       IN = 1          |                       | 01
   * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
   */
  private static byte[] questionRoot() {
    ByteBuffer buf = ByteBuffer.allocate(5);
    buf.put((byte) 0);
    buf.putShort(A.value());
    buf.putShort(IN.value());
    return buf.array();
  }

  private static byte[] rfc1035example() {
    byte[] header = dnsHeader();
    byte[] qFIsiArpa = questionFIsiArpa();
    byte[] qFoo = questionFoo();
    byte[] qArpa = questionArpa();
    byte[] qRoot = questionRoot();

    ByteBuffer buf = ByteBuffer.allocate(DnsMessage.BUFFER_SIZE);
    buf.put(header);
    buf.put(qFIsiArpa);
    buf.put(qFoo);
    buf.put(qArpa);
    buf.put(qRoot);
    return buf.array();
  }

  private static DnsMessage dnsMessage() {
    var header = DnsHeader.builder().build();
    var labels = Label.list("example", "com");
    var question = new DnsQuestion(DnsType.A, DnsClass.IN, labels);
    return DnsMessage.builder()
        .header(header)
        .questions(List.of(question))
        .build();
  }

  private static DnsMessage dnsMessages2() {
    var header = DnsHeader.builder().build();
    var labels = Label.list("example", "com");
    var q1 = new DnsQuestion(DnsType.A, DnsClass.IN, labels);
    var question2 = new DnsQuestion(DnsType.MX, DnsClass.CS, Label.list("codecrafters", "com"));
    return DnsMessage.builder()
        .header(header)
        .questions(List.of(q1, question2))
        .build();
  }

  @Test
  @DisplayName("Parse question: f.isi.arpa uncompressed")
  void parse_1() {
    ByteBuffer data = ByteBuffer.allocate(DnsMessage.BUFFER_SIZE);
    data.put(dnsHeader());
    data.put(questionFIsiArpa());
    var header = DnsHeaderParser.parse(data.array());
    var parser = new DnsQuestionParser(header, data.array());

    var dnsQuestion = parser.parseQuestion(12);

    assertNotNull(dnsQuestion);
    assertEquals(A, dnsQuestion.getType());
    assertEquals(IN, dnsQuestion.getDnsClass());
    assertEquals(Label.list("f", "isi", "arpa"), dnsQuestion.getName());
  }

  @Test
  @DisplayName("Parse question: foo, pointer to f.isi.arpa")
  void parse_2() {
    ByteBuffer data = ByteBuffer.allocate(DnsMessage.BUFFER_SIZE);
    data.put(dnsHeader());
    data.put(questionFIsiArpa());
    data.put(questionFoo());
    var header = DnsHeaderParser.parse(data.array());
    var parser = new DnsQuestionParser(header, data.array());

    var dnsQuestion = parser.parseQuestion(28);

    assertNotNull(dnsQuestion);
    assertEquals(A, dnsQuestion.getType());
    assertEquals(IN, dnsQuestion.getDnsClass());
    assertEquals(Label.list("foo", "f", "isi", "arpa"), dnsQuestion.getName());
  }

  @Test
  @DisplayName("Parse question: pointer to arpa")
  void parse_3() {
    ByteBuffer data = ByteBuffer.allocate(DnsMessage.BUFFER_SIZE);
    data.put(dnsHeader());
    data.put(questionFIsiArpa());
    data.put(questionFoo());
    data.put(questionArpa());
    var header = DnsHeaderParser.parse(data.array());
    var parser = new DnsQuestionParser(header, data.array());

    var dnsQuestion = parser.parseQuestion(38);

    assertNotNull(dnsQuestion);
    assertEquals(A, dnsQuestion.getType());
    assertEquals(IN, dnsQuestion.getDnsClass());
    assertEquals(Label.list("arpa"), dnsQuestion.getName());
  }

  @Test
  @DisplayName("Parsing RFC 1035 example message with pointers")
  void parseRfc1035example() {
    byte[] message = rfc1035example();
    var header = DnsHeaderParser.parse(message);
    var parser = new DnsQuestionParser(header, message);

    List<DnsQuestion> questions = parser.parseToList();

    assertEquals(4, questions.size());
    assertEquals(new DnsQuestion(A, IN, "f", "isi", "arpa"), questions.getFirst());
    assertEquals(new DnsQuestion(A, IN, "foo", "f", "isi", "arpa"), questions.get(1));
    assertEquals(new DnsQuestion(A, IN, "arpa"), questions.get(2));
    assertEquals(DnsQuestion.root(), questions.get(3));
  }

  @Test
  void questionsCount() {
    var message = dnsMessage();

    assertEquals(1, message.getHeader().getQuestionCount());
  }

  @Test
  void oneQuestionParsed() {
    var message = dnsMessage();
    byte[] messagePacket = ByteArray.fromDnsMessage(message);
    var header = DnsHeaderParser.parse(messagePacket);
    var parser = new DnsQuestionParser(header, messagePacket);

    var list = parser.parseToList();

    assertNotNull(list);
    assertEquals(1, list.size());
    var question = list.getFirst();
    assertEquals(DnsType.A, question.getType());
    assertEquals(DnsClass.IN, question.getDnsClass());
  }

  @Test
  void twoQuestionsParsed() {
    var message = dnsMessages2();
    byte[] messagePacket = ByteArray.fromDnsMessage(message);
    var header = DnsHeaderParser.parse(messagePacket);
    var parser = new DnsQuestionParser(header, messagePacket);

    var list = parser.parseToList();

    assertNotNull(list);
    assertEquals(2, list.size());
    var q1 = list.getFirst();
    assertEquals(DnsType.A, q1.getType());
    assertEquals(DnsClass.IN, q1.getDnsClass());
    var q2 = list.get(1);
    assertEquals(DnsType.MX, q2.getType());
    assertEquals(DnsClass.CS, q2.getDnsClass());
  }
}
