package parser;

import converter.ByteArray;
import dns.*;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DnsAnswerParserTest {

  private static byte[] answerData() {
    ByteBuffer buf = ByteBuffer.allocate(27);
    buf.put((byte) 7);
    buf.put(ByteArray.fromString("example"));
    buf.put((byte) 3);
    buf.put(ByteArray.fromString("com"));
    buf.put(DnsMessage.NULL_TERMINATOR);
    buf.putShort(DnsType.A.value());
    buf.putShort(DnsClass.IN.value());
    buf.putInt(60);
    byte[] data = new byte[]{8, 8, 8, 8};
    buf.putShort((short) data.length);
    buf.put(data);
    return buf.array();
  }

  private static DnsAnswer responseAnswer() {
    var ipData = new byte[]{8, 8, 8, 8};
    return DnsAnswer.builder()
        .name(Label.list("example", "com"))
        .type(DnsType.A)
        .dnsClass(DnsClass.IN)
        .timeToLive(60)
        .dataLength((short) ipData.length)
        .data(ipData)
        .build();
  }

  @Test
  void parse_1() {
    var header = DnsHeader.builder()
        .answerRecordCount((short) 1)
        .build();
    byte[] data = answerData();

    var parsed = DnsAnswerParser.parseToList(header, data, 0);

    assertNotNull(parsed);
    assertEquals(1, parsed.size());
    assertEquals(responseAnswer(), parsed.getFirst());
  }
}
