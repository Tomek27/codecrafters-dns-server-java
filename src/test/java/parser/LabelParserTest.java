package parser;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LabelParserTest {

  @ParameterizedTest
  @CsvSource({"-64,0,0", "-63,0,256", "-62,0,512", "-60,0,1024", "-58,0,1536", "-32,0,8192", "-16,0,12288", "-8,0,14336"})
  void pointerOffset(byte msb, byte lsb, int expected) {
    var parsed = LabelParser.pointerOffset(msb, lsb);

    assertEquals(expected, parsed);
  }

  @Test
  void pointerOffset_1() {
    var parsed = LabelParser.pointerOffset((byte) -64, (byte) 18);
    assertEquals(18, parsed);
  }

  @ParameterizedTest
  @CsvSource({"-64,true", "-63,true", "-32,true", "-16,true", "-8,true", "-4,true", "-65,false", "0,false"})
  void isPointer(String input, String expected) {
    byte value = Byte.parseByte(input);
    boolean expect = Boolean.parseBoolean(expected);

    var parsed = LabelParser.isPointer(value);

    assertEquals(expect, parsed);
  }
}
