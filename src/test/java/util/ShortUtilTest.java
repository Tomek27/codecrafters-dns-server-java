package util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ShortUtilTest {

  @ParameterizedTest
  @DisplayName("""
      GIVEN byte [{0}]\s
      WHEN fromBytes is called\s
      THEN the number {1} should be result
      """)
  @CsvSource(value = {
      "0,0,0", "0,1,1", "0,2,2", "0,4,4", "0,8,8", "0,16,16", "0,32,32", "0,64,64", "0,127,127"})
  void fromBytes_1(byte msb, byte lsb, short expected) {
    var parsed = ShortUtil.fromBytes(msb, lsb);
    assertEquals(expected, parsed);
  }

  @Test
  void fromBytes_10() {
    byte[] value = new byte[]{1, 0};
    var parsed = ShortUtil.fromBytes(value[0], value[1]);
    assertEquals(256, parsed);
  }

  @Test
  void fromBytes_11() {
    byte[] value = new byte[]{1, 1};
    var parsed = ShortUtil.fromBytes(value[0], value[1]);
    assertEquals(257, parsed);
  }

  @Test
  void fromBytes_0neg1() {
    byte[] value = new byte[]{0, -1};
    var parsed = ShortUtil.fromBytes(value[0], value[1]);
    assertEquals(255, parsed);
  }

  @Test
  void fromBytes_0neg128() {
    byte[] value = new byte[]{0, -128};
    var parsed = ShortUtil.fromBytes(value[0], value[1]);
    assertEquals(128, parsed);
  }

  @Test
  void fromBytes_1neg128() {
    byte[] value = new byte[]{1, -128};
    var parsed = ShortUtil.fromBytes(value[0], value[1]);
    assertEquals(384, parsed);
  }
}
