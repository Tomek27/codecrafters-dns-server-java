package util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegerUtilTest {

  @ParameterizedTest
  @CsvSource({"0,0", "1,1", "2,2", "4,4", "8,8", "16,16", "32,32", "64,64"})
  void fromByteBitwise_0to64(String input, String expected) {
    byte value = Byte.parseByte(input);
    int parsed = IntegerUtil.fromByteBitwise(value);
    int expect = Integer.parseInt(expected);
    assertEquals(expect, parsed);
  }

  @Test
  void fromByteBitwise_127() {
    int parsed = IntegerUtil.fromByteBitwise((byte) 127);
    assertEquals(127, parsed);
  }

  @Test
  void fromByteBitwise_neg128() {
    int parsed = IntegerUtil.fromByteBitwise((byte) -128);
    assertEquals(128, parsed);
  }

  @Test
  void fromByteBitwise_neg127() {
    int parsed = IntegerUtil.fromByteBitwise((byte) -127);
    assertEquals(129, parsed);
  }

  @Test
  void fromByteBitwise_neg1() {
    int parsed = IntegerUtil.fromByteBitwise((byte) -1);
    assertEquals(255, parsed);
  }

  @Test
  void fromByteBitwise_neg2() {
    int parsed = IntegerUtil.fromByteBitwise((byte) -2);
    assertEquals(254, parsed);
  }

  @Test
  void fromByteBitwise_neg64() {
    int parsed = IntegerUtil.fromByteBitwise((byte) -64);
    assertEquals(192, parsed);
  }

  @Test
  void fromFourBytesOffset() {
    byte[] data = new byte[]{1, 1, 1, 1};
    int parsed = IntegerUtil.fromFourBytesOffset(data, 0);
    assertEquals(4369, parsed);
  }
}
