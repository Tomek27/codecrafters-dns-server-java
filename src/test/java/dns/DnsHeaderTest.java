package dns;

import converter.ByteArray;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import parser.DnsHeaderParser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DnsHeaderTest {

  @Test
  @DisplayName("""
      GIVEN a build header with id 1234\s
      WHEN header is converted to byte array and parsed back\s
      THEN the same package id should be present
      """)
  void parseHeader_1() {
    var header = DnsHeader.builder()
        .packetIdentifier((short) 1234)
        .build();
    var headerData = ByteArray.fromDnsHeader(header);

    var parsed = DnsHeaderParser.parse(headerData);

    assertEquals(header.getPacketIdentifier(), parsed.getPacketIdentifier());
  }

  @Test
  @DisplayName("""
      GIVEN a build header with truncation set to true/1\s
      WHEN header is converted to byte array and parsed back\s
      THEN the truncation flag should be set true/1
      """)
  void parseHeader_2() {
    var header = DnsHeader.builder()
        .truncation(true)
        .build();
    var headerData = ByteArray.fromDnsHeader(header);

    var parsed = DnsHeaderParser.parse(headerData);

    assertTrue(parsed.isTruncation());
  }

  @Test
  @DisplayName("""
      GIVEN a build header with response code 4\s
      WHEN header is converted to byte array and parsed back\s
      THEN the response code will be 4
      """)
  void parseHeader_3() {
    var header = DnsHeader.builder()
        .responseCode(ResponseCode.NOT_IMPLEMENTED)
        .build();
    var headerData = ByteArray.fromDnsHeader(header);

    var parsed = DnsHeaderParser.parse(headerData);

    assertEquals(ResponseCode.NOT_IMPLEMENTED, parsed.getResponseCode());
  }

  @Test
  @DisplayName("""
      GIVEN a build header with operation code 1\s
      WHEN header is converted to byte array and parsed back\s
      THEN the operation code will be 1
      """)
  void parseHeader_4() {
    var header = DnsHeader.builder()
        .operationCode((byte) 1)
        .build();
    var headerData = ByteArray.fromDnsHeader(header);

    var parsed = DnsHeaderParser.parse(headerData);

    assertEquals(1, parsed.getOperationCode());
  }
}
