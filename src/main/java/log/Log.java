package log;

public class Log {

  static Log INSTANCE;

  public static Log getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new Log();
    }
    return INSTANCE;
  }

  public void log(String format, Object... args) {
    System.out.printf(format, args);
    System.out.println();
  }

  private Log() {}
}
