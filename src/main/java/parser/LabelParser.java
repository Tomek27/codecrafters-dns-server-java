package parser;

import config.Config;
import dns.DnsMessage;
import dns.Label;
import log.Log;
import util.ShortUtil;
import util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class LabelParser {

  static final int POINTER_MARK_MASK = 0xC000;
  static final int POINTER_OFFSET_MASK = 0x3FFF;
  private static final Log log = Log.getInstance();
  private final List<Label> name = new ArrayList<>();
  private final byte[] data;
  private int offset;
  private int uncompressedFinishedOffset = -1;
  private int pointerStartedOffset = -1;

  LabelParser(byte[] data, int initOffset) {
    this.data = data;
    this.offset = initOffset;
  }

  public static boolean isPointer(byte msb) {
    int value = ShortUtil.fromBytes(msb, (byte) 0);
    int masked = POINTER_MARK_MASK & value;
    return masked == POINTER_MARK_MASK;
  }

  static int pointerOffset(byte msb, byte lsb) {
    int value = ShortUtil.fromBytes(msb, lsb);
    log.log("%d, %d = %d = %s", msb, lsb, value, StringUtil.byteArrayToTwoOctets(new byte[]{msb, lsb}));
    int offset = POINTER_OFFSET_MASK & value;
    log.log("%d = %s", offset, StringUtil.shortToFourOctets((short) offset));
    return offset;
  }

  List<Label> parseToList() {
    parseUncompressed();
    parsePointer();
    return name;
  }

  int getIdxNameEnd() {
    if (pointerStartedOffset > -1) {
      return pointerStartedOffset += 1;
    } else {
      return uncompressedFinishedOffset;
    }
  }

  void parseUncompressed() {
    while (offset >= 0 && isNotLabelEnd(offset) && isNotPointer(offset)) {
      offset = parseLabelAndReturnNextOffset(offset);
    }
    uncompressedFinishedOffset = offset;
  }

  void parsePointer() {
    if (isPointer(data[offset])) {
      pointerStartedOffset = offset;
      offset = pointerOffset(data[offset], data[offset + 1]);
      parseUncompressed();
    }
  }

  private boolean isNotPointer(int offset) {
    return !isPointer(data[offset]);
  }

  private int parseLabelAndReturnNextOffset(int offset) {
    int labelLength = data[offset];
    int labelStart = offset + 1;
    byte[] labelData = new byte[labelLength];
    System.arraycopy(data, labelStart, labelData, 0, labelLength);

    Label label = new Label(new String(labelData, Config.CHARSET));
    name.add(label);

    return labelStart + labelLength;
  }

  private boolean isNotLabelEnd(int offset) {
    return !(data[offset] == DnsMessage.NULL_TERMINATOR);
  }
}
