package parser;

import dns.*;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class DnsQuestionParser {

  private final List<DnsQuestion> questions = new ArrayList<>();
  private final DnsHeader header;
  private final byte[] data;
  private int nextQuestionOffset = -1;
  @Getter
  private int answersOffset = -1;

  DnsQuestionParser(DnsHeader header, byte[] data) {
    this.header = header;
    this.data = data;
  }

  List<DnsQuestion> parseToList() {
    int countQuestions = header.getQuestionCount();
    int nextOffset = DnsHeader.LENGTH_BYTE;

    for (int i = 0; i < countQuestions; ++i) {
      DnsQuestion question = parseQuestion(nextOffset);
      questions.add(question);
      nextOffset = nextQuestionOffset;
    }
    answersOffset = nextOffset;

    return questions;
  }

  DnsQuestion parseQuestion(int offset) {
    LabelParser parser = new LabelParser(data, offset);
    List<Label> name = parser.parseToList();
    int idxNameEnd = parser.getIdxNameEnd();
    DnsType type = ParserUtils.getType(data, idxNameEnd);
    DnsClass dnsClass = ParserUtils.getDnsClass(data, idxNameEnd);
    nextQuestionOffset = idxNameEnd + 5;
    return new DnsQuestion(type, dnsClass, name);
  }
}
