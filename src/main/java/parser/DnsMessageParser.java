package parser;

import dns.DnsMessage;

public class DnsMessageParser {

  private DnsMessageParser() {
  }

  public static DnsMessage parse(byte[] data) {
    var header = DnsHeaderParser.parse(data);
    var questionsParser = new DnsQuestionParser(header, data);
    var questions = questionsParser.parseToList();
    var answers = DnsAnswerParser.parseToList(header, data, questionsParser.getAnswersOffset());
    return DnsMessage.builder()
        .header(header)
        .questions(questions)
        .answers(answers)
        .build();
  }
}
