package parser;

import dns.DnsHeader;
import util.ShortUtil;

public class DnsHeaderParser {

  private DnsHeaderParser() {
  }

  public static DnsHeader parse(byte[] data) {
    short packetIdentifier = ShortUtil.fromBytes(data[0], data[1]);
    short flags = ShortUtil.fromBytes(data[2], data[3]);
    short questionCount = ShortUtil.fromBytes(data[4], data[5]);
    short answerRecordCount = ShortUtil.fromBytes(data[6], data[7]);
    short authorityRecordCount = ShortUtil.fromBytes(data[8], data[9]);
    short additionalRecordCount = ShortUtil.fromBytes(data[10], data[11]);
    return DnsHeader.builder()
        .packetIdentifier(packetIdentifier)
        .flags(flags)
        .questionCount(questionCount)
        .answerRecordCount(answerRecordCount)
        .authorityRecordCount(authorityRecordCount)
        .additionalRecordCount(additionalRecordCount)
        .build();
  }
}
