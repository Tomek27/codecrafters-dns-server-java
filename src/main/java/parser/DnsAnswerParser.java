package parser;

import dns.*;
import util.IntegerUtil;
import util.ShortUtil;

import java.util.ArrayList;
import java.util.List;

public class DnsAnswerParser {

  private final List<DnsAnswer> answers = new ArrayList<>();
  private final byte[] data;
  private int nextAnswerOffset = -1;

  DnsAnswerParser(byte[] data) {
    this.data = data;
  }

  public static List<DnsAnswer> parseToList(DnsHeader header, byte[] data, int initOffset) {
    var parser = new DnsAnswerParser(data);
    int nextOffset = initOffset;

    for (int i = 0; i < header.getAnswerRecordCount(); ++i) {
      DnsAnswer answer = parser.parseAnswer(nextOffset);
      parser.answers.add(answer);
      nextOffset = parser.nextAnswerOffset;
    }

    return parser.answers;
  }

  private DnsAnswer parseAnswer(int offset) {
    LabelParser parser = new LabelParser(data, offset);
    List<Label> name = parser.parseToList();
    int idxNameEnd = parser.getIdxNameEnd();
    DnsType type = ParserUtils.getType(data, idxNameEnd);
    DnsClass dnsClass = ParserUtils.getDnsClass(data, idxNameEnd);
    int idxTtl = idxNameEnd + 5;
    int ttl = IntegerUtil.fromFourBytesOffset(data, idxTtl);
    int idxDataLength = idxTtl + 4;
    short dataLength = ShortUtil.fromBytes(data[idxDataLength], data[idxDataLength + 1]);
    byte[] answerData = new byte[dataLength];
    int idxData = idxDataLength + 2;
    System.arraycopy(data, idxData, answerData, 0, dataLength);
    nextAnswerOffset = idxData + dataLength;
    return DnsAnswer.builder()
        .name(name)
        .type(type)
        .dnsClass(dnsClass)
        .timeToLive(ttl)
        .dataLength(dataLength)
        .data(answerData)
        .build();
  }
}
