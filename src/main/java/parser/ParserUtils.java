package parser;

import dns.DnsClass;
import dns.DnsType;
import util.ShortUtil;

class ParserUtils {

  private ParserUtils() {
  }

  static DnsType getType(byte[] data, int idxNameEnd) {
    short type = ShortUtil.fromBytes(data[idxNameEnd + 1], data[idxNameEnd + 2]);
    return DnsType.fromValue(type);
  }

  static DnsClass getDnsClass(byte[] data, int idxNameEnd) {
    short type = ShortUtil.fromBytes(data[idxNameEnd + 3], data[idxNameEnd + 4]);
    return DnsClass.fromValue(type);
  }
}
