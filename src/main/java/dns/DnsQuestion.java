package dns;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class DnsQuestion {

  private final List<Label> name;

  private final DnsType type;

  private final DnsClass dnsClass;

  public DnsQuestion(DnsType type, DnsClass dnsClass, String... labels) {
    this(type, dnsClass, Label.list(labels));
  }

  public DnsQuestion(DnsType type, DnsClass dnsClass, List<Label> labels) {
    Objects.requireNonNull(type, "Type cannot be null");
    Objects.requireNonNull(dnsClass, "DnsClass cannot be null");
    Objects.requireNonNull(labels, "Labels cannot be null");
    this.type = type;
    this.dnsClass = dnsClass;
    this.name = labels;
  }

  public static DnsQuestion root() {
    return new DnsQuestion(DnsType.A, DnsClass.IN, new ArrayList<>());
  }
}
