package dns;

public enum ResponseCode {
  NO_ERROR(0),
  NOT_IMPLEMENTED(4);

  private final int value;

  ResponseCode(int value) {
    this.value = value;
  }

  public static ResponseCode fromValue(int value) {
    for (ResponseCode code : ResponseCode.values()) {
      if (code.value == value) {
        return code;
      }
    }

    throw new IllegalArgumentException("Unknown value for Response Code: " + value);
  }

  public byte value() {
    return (byte) value;
  }
}
