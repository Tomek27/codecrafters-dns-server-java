package dns;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DnsMessage {

  public static final int BUFFER_SIZE = 512;
  public static final byte NULL_TERMINATOR = (byte) 0;

  private DnsHeader header;
  private List<DnsQuestion> questions;
  private List<DnsAnswer> answers;

  public DnsAnswer getFirstAnswer() {
    return answers != null ? answers.getFirst() : null;
  }

  public static class DnsMessageBuilder {
    public DnsMessageBuilder questions(List<DnsQuestion> questions) {
      this.questions = questions;
      short count = (short) (questions != null ? questions.size() : 0);
      this.header.setQuestionCount(count);
      return this;
    }

    public DnsMessageBuilder answers(List<DnsAnswer> answers) {
      this.answers = answers;
      short count = (short) (answers != null ? answers.size() : 0);
      this.header.setAnswerRecordCount(count);
      return this;
    }
  }
}
