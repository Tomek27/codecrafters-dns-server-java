package dns;

import java.util.ArrayList;
import java.util.List;

public record Label(String content) {

  public static List<Label> list(String... labels) {
    List<Label> name = new ArrayList<>(labels.length);
    for (String label : labels) {
      name.add(new Label(label));
    }
    return name;
  }

  public int bytesLength() {
    return content.length() + 1;
  }
  
}
