package dns;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DnsHeader {

  public static final byte RESPONSE_CODE_NOT_IMPLEMENTED = 4;

  public static final int LENGTH_BYTE = 12;

  private static final int QUERY_RESPOND_INDICATOR_MASK = 0b1000_0000_0000_0000;
  private static final short OPERATION_CODE_OFFSET = 11;
  private static final short OPERATION_CODE_MASK = 0b0111_1000_0000_0000;
  private static final short AUTHORITATIVE_ANSWER_MASK = 0b0000_0100_0000_0000;
  private static final short TRUNCATION_MASK = 0b0000_0010_0000_0000;
  private static final short RECURSION_DESIRED_MASK = 0b0000_0001_0000_0000;
  private static final short RECURSION_AVAILABLE_MASK = 0b0000_0000_1000_0000;
  private static final short RESERVED_OFFSET = 4;
  private static final short RESERVED_MASK = 0b0000_0000_0111_0000;
  private static final short RESPOND_CODE_MASK = 0b0000_0000_0000_1111;

  // Packet Identifier (ID) 16bit
  private short packetIdentifier;
  // Query/Response Indicator (QR) 1bit
  private boolean queryResponseIndicator;
  // Operation Code (OPCODE)
  private byte operationCode;
  // Authoritative Answer (AA) 1bit
  private boolean authoritativeAnswer;
  // Truncation (TC) 1bit
  private boolean truncation;
  // Recursion Desired (RD) 1bit
  private boolean recursionDesired;
  // Recursion Available (RA) 1bit
  private boolean recursionAvailable;
  // Reserved (Z)
  private byte reserved;
  // Response Code (RCODE)
  private ResponseCode responseCode;
  // Question Count (QDCOUNT) 16bit
  private short questionCount;
  // Answer Record Count (ANCOUNT) 16bit
  private short answerRecordCount;
  // Authority Record Count (NSCOUNT) 16bit
  private short authorityRecordCount;
  // Additional Record Count (ARCOUNT) 16bit
  private short additionalRecordCount;

  public short getFlags() {
    int flags = 0;
    if (queryResponseIndicator) flags = flags ^ QUERY_RESPOND_INDICATOR_MASK;
    int operationCodeShifted = operationCode << OPERATION_CODE_OFFSET;
    flags = flags ^ operationCodeShifted;
    if (authoritativeAnswer) flags = flags ^ AUTHORITATIVE_ANSWER_MASK;
    if (truncation) flags = flags ^ TRUNCATION_MASK;
    if (recursionDesired) flags = flags ^ RECURSION_DESIRED_MASK;
    if (recursionAvailable) flags = flags ^ RECURSION_AVAILABLE_MASK;
    int reservedShifted = reserved << RESERVED_OFFSET;
    flags = flags ^ reservedShifted;
    if (responseCode == null) responseCode = ResponseCode.NOT_IMPLEMENTED;
    flags = flags ^ responseCode.value();
    return (short) flags;
  }

  public boolean isReplyPacket() {
    return queryResponseIndicator;
  }

  public boolean isQuestionPacket() {
    return !queryResponseIndicator;
  }

  public boolean isRespondingServerOwningDomain() {
    return authoritativeAnswer;
  }

  public static class DnsHeaderBuilder {
    public DnsHeaderBuilder flags(short flags) {
      int queryResponseIndicator = flags & QUERY_RESPOND_INDICATOR_MASK;
      int operationCodeMasked = flags & OPERATION_CODE_MASK;
      int operationCode = operationCodeMasked >>> OPERATION_CODE_OFFSET;
      int authoritativeAnswer = flags & AUTHORITATIVE_ANSWER_MASK;
      int truncation = flags & TRUNCATION_MASK;
      int recursionDesired = flags & RECURSION_DESIRED_MASK;
      int recursionAvailable = flags & RECURSION_AVAILABLE_MASK;
      int reservedMasked = flags & RESERVED_MASK;
      int reserved = reservedMasked >>> RESERVED_OFFSET;
      int responseCode = flags & RESPOND_CODE_MASK;
      return this.queryResponseIndicator(queryResponseIndicator > 0)
          .operationCode((byte) operationCode)
          .authoritativeAnswer(authoritativeAnswer > 0)
          .truncation(truncation > 0)
          .recursionDesired(recursionDesired > 0)
          .recursionAvailable(recursionAvailable > 0)
          .reserved((byte) reserved)
          .responseCode(ResponseCode.fromValue(responseCode));
    }
  }
}
