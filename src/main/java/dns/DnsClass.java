package dns;

public enum DnsClass {
  /**
   * internet
   */
  IN(1),
  /**
   * CSNET class (obsolete - used only for examples in obsolete RFCs)
   */
  CS(2),
  /**
   * CHAOS class
   */
  CH(3),
  /**
   * Hesiod [Dyer 87]
   */
  HS(4);

  public static final int LENGTH = 2;

  private final int value;

  DnsClass(int value) {
    this.value = value;
  }

  public static DnsClass fromValue(int value) {
    for (DnsClass c : DnsClass.values()) {
      if (c.value == value) {
        return c;
      }
    }
    throw new IllegalArgumentException("Unknown value for DNS class: " + value);
  }

  public short value() {
    return (short) value;
  }
}
