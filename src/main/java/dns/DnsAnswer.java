package dns;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class DnsAnswer {

  public static final int TIME_TO_LIVE_LENGTH = 4;
  public static final int LENGTH_LENGTH = 2;
  public static final int IP_LENGTH = 4;

  private final List<Label> name;

  private final DnsType type;

  private final DnsClass dnsClass;

  private final int timeToLive;

  private final short dataLength;

  private final byte[] data;

  public static DnsAnswer empty(DnsType type, DnsClass dnsClass) {
    return DnsAnswer.builder()
        .name(new ArrayList<>())
        .type(type)
        .dnsClass(dnsClass)
        .timeToLive(0)
        .dataLength((short) 0)
        .data(new byte[0])
        .build();
  }
}
