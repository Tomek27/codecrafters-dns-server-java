package dns;

public enum DnsType {
  /**
   * host address
   */
  A(1),
  /**
   * authoritative name server
   */
  NS(2),
  /**
   * mail destination (obsolete - use MX)
   */
  MD(3),
  /**
   * mail forwarder (obsolete - use MX)
   */
  MF(4),
  /**
   * canonical name for an alias
   */
  CNAME(5),
  /**
   * marks the start of a zone of authority
   */
  SOA(6),
  /**
   * mailbox domain name (experimental)
   */
  MB(7),
  /**
   * mail group member (experimental)
   */
  MG(8),
  /**
   * mail rename domain name (experimental)
   */
  MR(9),
  /**
   * null RR (experimental)
   */
  NULL(10),
  /**
   * well known service description
   */
  WKS(11),
  /**
   * domain name pointer
   */
  PTR(12),
  /**
   * host information
   */
  HINFO(13),
  /**
   * mailbox or mail list information
   */
  MINFO(14),
  /**
   * mail exchange
   */
  MX(15),
  /**
   * text strings
   */
  TXT(16);

  public static final int LENGTH = 2;

  private final int value;

  DnsType(int value) {
    this.value = value;
  }

  public static DnsType fromValue(int value) {
    for (DnsType type : DnsType.values()) {
      if (type.value == value) {
        return type;
      }
    }

    throw new IllegalArgumentException("Unknown value for DNS type: " + value);
  }

  public short value() {
    return (short) value;
  }
}
