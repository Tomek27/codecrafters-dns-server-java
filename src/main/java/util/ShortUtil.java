package util;

public class ShortUtil {

  private ShortUtil() {
  }

  public static short fromBytes(byte msb, byte lsb) {
    int part0 = IntegerUtil.fromByteBitwise(msb);
    int part0shifted = part0 << 8;
    int part1 = IntegerUtil.fromByteBitwise(lsb);
    int combined = part0shifted + part1;
    return (short) combined;
  }

}
