package util;

public class IntegerUtil {

  private IntegerUtil() {
  }

  /**
   * @param val bits from byte
   * @return Integer value parsed bitwise, ignoring sign
   */
  public static int fromByteBitwise(byte val) {
    int num = 0;

    for (int i = 0; i < 8; ++i) {
      int mask = 1 << i;
      int masked = mask & val;
      if (masked > 0) num += mask;
    }

    return num;
  }

  public static int fromFourBytesOffset(byte[] data, int offset) {
    int msb0 = fromByteBitwise(data[offset]);
    int msb1 = fromByteBitwise(data[offset + 1]);
    int msb2 = fromByteBitwise(data[offset + 2]);
    int num = fromByteBitwise(data[offset + 3]);
    
    num += (msb2 << 4);
    num += (msb1 << 8);
    num += (msb0 << 12);

    return num;
  }
}
