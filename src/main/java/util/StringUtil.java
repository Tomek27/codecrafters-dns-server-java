package util;

public class StringUtil {

  private StringUtil() {
  }

  public static String byteArrayToTwoOctets(byte[] array) {
    StringBuilder b = new StringBuilder();
    for (int i = 0; i < array.length - 1; i += 2) {
      b.append(byteToTowOctets(array[i]));
      b.append(byteToTowOctets(array[i + 1]));
      b.append('\n');
    }
    return b.toString();
  }

  public static String byteToTowOctets(byte value) {
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < Byte.SIZE; ++i) {
      int mask = 1 << i;
      int masked = mask & value;
      if (i % 4 == 0) s.append(' ');
      s.append(masked > 0 ? "1" : "0");
    }
    return s.reverse().toString();
  }

  public static String shortToFourOctets(short value) {
    StringBuilder s = new StringBuilder();
    for (int i = 0; i < Short.SIZE; ++i) {
      int mask = 1 << i;
      int masked = mask & value;
      if (i % 4 == 0) s.append(' ');
      s.append(masked > 0 ? "1" : "0");
    }
    return s.reverse().toString();
  }
}
