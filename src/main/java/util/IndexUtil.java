package util;

import parser.LabelParser;

import static dns.DnsMessage.NULL_TERMINATOR;

public class IndexUtil {

  private IndexUtil() {
  }

  public static int findNextNullTerminator(byte[] data, int offset) {
    int idxNull = -1;
    for (int i = offset; i < data.length; ++i) {
      if (data[i] == NULL_TERMINATOR) {
        idxNull = i;
        break;
      }
    }
    return idxNull;
  }

  public static int findNextPointer(byte[] data, int offset) {
    int idxPointer = -1;
    for (int i = offset; i < data.length; ++i) {
      if (LabelParser.isPointer(data[i])) {
        idxPointer = i;
        break;
      }
    }
    return idxPointer;
  }
}
