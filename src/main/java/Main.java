import config.Config;
import converter.DatagramPacketConverter;
import dns.*;
import log.Log;
import parser.DnsMessageParser;
import resolver.ResolverClient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Collection;

public class Main {

  private static final Log log = Log.getInstance();

  public static void main(String[] args) {
    Config config = Config.parse(args);

    try (DatagramSocket serverSocket = new DatagramSocket(config.getPort())) {
      log.log("DNS listening on port: %d", config.getPort());
      log.log("DNS forwarding resolver address: %s", config.getResolverAddress());
      while (true) {
        final byte[] buf = new byte[DnsMessage.BUFFER_SIZE];
        final DatagramPacket requestPackage = new DatagramPacket(buf, buf.length);
        serverSocket.receive(requestPackage);
        log.log("Package received from: %s", requestPackage.getAddress());

        var request = parseRequest(requestPackage.getData());

        DnsMessage response;
        if (config.hasResolverAddress()) {
          response = resolveResponse(request, config);
        } else {
          response = createResponse(request);
        }
        log.log("Response: %s", response);

        var responsePacket = DatagramPacketConverter.fromDnsMessage(response, requestPackage.getSocketAddress());
        serverSocket.send(responsePacket);
      }
    } catch (IOException e) {
      log.log("IOException: " + e.getMessage());
    }
  }

  private static DnsMessage parseRequest(byte[] dataPackage) {
    var message = DnsMessageParser.parse(dataPackage);
    log.log("Request: %s", message);
    return message;
  }

  private static DnsMessage createResponse(DnsMessage request) {
    var header = DnsHeader.builder()
        .packetIdentifier(request.getHeader().getPacketIdentifier())
        .queryResponseIndicator(true)
        .operationCode(request.getHeader().getOperationCode())
        .responseCode(ResponseCode.NOT_IMPLEMENTED)
        .recursionDesired(request.getHeader().isRecursionDesired())
        .build();

    var questions = request.getQuestions().stream().map(Main::responseQuestion).toList();
    var answers = request.getQuestions().stream().map(Main::responseAnswer).toList();

    return DnsMessage.builder()
        .header(header).questions(questions).answers(answers)
        .build();
  }

  private static DnsMessage resolveResponse(DnsMessage request, Config config) {
    var answers = request.getQuestions().stream()
        .map(question -> ResolverClient.resolve(question, request.getHeader(), config))
        .flatMap(Collection::stream)
        .toList();

    var questions = request.getQuestions();

    var header = DnsHeader.builder()
        .packetIdentifier(request.getHeader().getPacketIdentifier())
        .queryResponseIndicator(true)
        .operationCode(request.getHeader().getOperationCode())
        .responseCode(ResponseCode.NOT_IMPLEMENTED)
        .recursionDesired(request.getHeader().isRecursionDesired())
        .build();

    return DnsMessage.builder()
        .header(header).questions(questions).answers(answers)
        .build();
  }

  private static DnsQuestion responseQuestion(DnsQuestion request) {
    return new DnsQuestion(DnsType.A, DnsClass.IN, request.getName());
  }

  private static DnsAnswer responseAnswer(DnsQuestion question) {
    var ipData = new byte[]{8, 8, 8, 8};
    return DnsAnswer.builder()
        .name(question.getName())
        .type(question.getType())
        .dnsClass(question.getDnsClass())
        .timeToLive(60)
        .dataLength((short) ipData.length)
        .data(ipData)
        .build();
  }
}
