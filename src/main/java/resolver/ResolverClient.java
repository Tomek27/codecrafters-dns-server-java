package resolver;

import config.Config;
import converter.DatagramPacketConverter;
import dns.DnsAnswer;
import dns.DnsHeader;
import dns.DnsMessage;
import dns.DnsQuestion;
import log.Log;
import parser.DnsMessageParser;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class ResolverClient {

  private static final Log log = Log.getInstance();

  public static List<DnsAnswer> resolve(DnsQuestion question, DnsHeader header, Config config) {
    var request = DnsMessage.builder()
        .header(header)
        .questions(List.of(question))
        .build();
    log.log("Resolve request: %s", request);
    var forwardMessage = DatagramPacketConverter.fromDnsMessage(request, config.getResolverAddress());

    try (var socket = new DatagramSocket()) {
      socket.send(forwardMessage);

      var buffer = new byte[DnsMessage.BUFFER_SIZE];
      var responsePacket = new DatagramPacket(buffer, buffer.length);
      socket.receive(responsePacket);

      var response = DnsMessageParser.parse(buffer);
      log.log("Resolve response: %s", response);
      return response.getAnswers();
    } catch (SocketException e) {
      log.log("Could not resolve id: %d\n%s", request.getHeader().getPacketIdentifier(), e.getMessage());
    } catch (IOException e) {
      log.log("Forwarding message id failed: %d\n%s", request.getHeader().getPacketIdentifier(), e.getMessage());
    }

    return new ArrayList<>();
  }
}
