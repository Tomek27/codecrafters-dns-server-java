package converter;

import dns.DnsMessage;

import java.net.DatagramPacket;
import java.net.SocketAddress;

public class DatagramPacketConverter {

  private DatagramPacketConverter() {
  }

  public static DatagramPacket fromDnsMessage(DnsMessage message, SocketAddress socketAddress) {
    final byte[] bufResponse = ByteArray.fromDnsMessage(message);
    return new DatagramPacket(bufResponse, bufResponse.length, socketAddress);
  }
}
