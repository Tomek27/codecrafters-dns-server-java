package converter;

import config.Config;
import dns.*;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;

import static dns.DnsAnswer.*;
import static dns.DnsHeader.LENGTH_BYTE;
import static dns.DnsMessage.NULL_TERMINATOR;

public class ByteArray {

  private ByteArray() {
  }

  public static byte[] fromString(String s) {
    return s.getBytes(Config.CHARSET);
  }

  public static byte[] fromDnsHeader(DnsHeader header) {
    ByteBuffer buffer = ByteBuffer.allocate(LENGTH_BYTE);
    buffer.putShort(header.getPacketIdentifier());
    buffer.putShort(header.getFlags());
    buffer.putShort(header.getQuestionCount());
    buffer.putShort(header.getAnswerRecordCount());
    buffer.putShort(header.getAuthorityRecordCount());
    buffer.putShort(header.getAdditionalRecordCount());
    return buffer.array();
  }

  public static byte[] fromDnsQuestion(DnsQuestion question) {
    int nameLength = question.getName().stream()
        .map(Label::bytesLength).reduce(0, Integer::sum);
    int totalLength = nameLength + 1 + DnsType.LENGTH + DnsClass.LENGTH;
    ByteBuffer buffer = ByteBuffer.allocate(totalLength);
    question.getName().forEach(label -> buffer.put(fromLabel(label)));
    buffer.put(NULL_TERMINATOR);
    buffer.putShort(question.getType().value());
    buffer.putShort(question.getDnsClass().value());
    return buffer.array();
  }

  public static byte[] fromDnsAnswer(DnsAnswer answer) {
    int nameLength = answer.getName().stream()
        .map(Label::bytesLength).reduce(0, Integer::sum);
    int totalLength = nameLength + 1 + DnsType.LENGTH + DnsClass.LENGTH + TIME_TO_LIVE_LENGTH + LENGTH_LENGTH + IP_LENGTH;
    ByteBuffer buffer = ByteBuffer.allocate(totalLength);
    answer.getName().forEach(label -> buffer.put(fromLabel(label)));
    buffer.put(NULL_TERMINATOR);
    buffer.putShort(answer.getType().value());
    buffer.putShort(answer.getDnsClass().value());
    buffer.putInt(answer.getTimeToLive());
    buffer.putShort(answer.getDataLength());
    buffer.put(answer.getData());
    return buffer.array();
  }

  public static byte[] fromDnsMessage(DnsMessage message) {
    byte[] headerBytes = ByteArray.fromDnsHeader(message.getHeader());
    ByteBuffer buffer = ByteBuffer.allocate(DnsMessage.BUFFER_SIZE);
    buffer.put(headerBytes);

    if (message.getQuestions() != null) {
      List<byte[]> questionsBytes = message.getQuestions().stream()
          .filter(Objects::nonNull)
          .map(ByteArray::fromDnsQuestion).toList();
      questionsBytes.forEach(buffer::put);
    }
    if (message.getAnswers() != null) {
      List<byte[]> answersBytes = message.getAnswers().stream()
          .filter(Objects::nonNull)
          .map(ByteArray::fromDnsAnswer).toList();
      answersBytes.forEach(buffer::put);
    }

    return buffer.array();
  }

  public static byte[] fromLabel(Label label) {
    ByteBuffer buffer = ByteBuffer.allocate(label.bytesLength());
    buffer.put((byte) label.content().length());
    buffer.put(label.content().getBytes(Config.CHARSET));
    return buffer.array();
  }
}
