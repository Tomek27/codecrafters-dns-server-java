package config;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

class ConfigOptions extends Options {

  static final Option PORT = Option.builder("p").longOpt("port").hasArg().desc("Port to listen").build();
  static final Option RESOLVER = Option.builder("r").longOpt("resolver").hasArg().desc("Address to forward the DNS message").build();

  ConfigOptions() {
    addOption(PORT);
    addOption(RESOLVER);
  }
}
