package config;

import log.Log;
import lombok.Getter;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static config.ConfigOptions.PORT;
import static config.ConfigOptions.RESOLVER;

@Getter
public class Config {

  public static final Charset CHARSET = StandardCharsets.UTF_8;

  private static final Log log = Log.getInstance();

  private int port = 2053;
  private InetSocketAddress resolverAddress;

  private Config() {
  }

  public static Config parse(String[] args) {
    var config = new Config();
    try {
      var parser = new DefaultParser();
      var commandLine = parser.parse(new ConfigOptions(), args);
      if (commandLine.hasOption(PORT)) {
        String portArg = commandLine.getOptionValue(PORT);
        config.port = Integer.parseInt(portArg);
      }
      if (commandLine.hasOption(RESOLVER)) {
        String resolverAddressArg = commandLine.getOptionValue(RESOLVER);
        config.resolverAddress = getInetSocketResolverAddress(resolverAddressArg);
      }
    } catch (ParseException e) {
      log.log("Parsing config: " + e.getMessage());
    }
    return config;
  }

  private static InetSocketAddress getInetSocketResolverAddress(String resolverAddress) {
    String[] split = resolverAddress.split(":");
    try {
      InetAddress address = InetAddress.getByName(split[0]);
      int port = Integer.parseInt(split[1]);
      return new InetSocketAddress(address, port);
    } catch (UnknownHostException e) {
      log.log("Parse resolver address: %s", e.getMessage());
      throw new RuntimeException(e);
    }
  }

  public boolean hasResolverAddress() {
    return resolverAddress != null;
  }
}
